NYMALL'S FOODLAB:

This mod is designed to extend the cooking recipies already in the base game with real world counterparts taken from popular cook books. The recipies have been averaged to provide a consistant method to interact with CDDA.

- Up to 1 cup = 1 unit
- Up to 1 Lb = 1 Lb
- any spices up to 200g = 1 unit

For refrigerated items that need to sit for a certain amount of time, this mod provides different tools so that these items are accessable early game. You will need an MTX socket and a Improvised or Commercial MTX item for the job. This creates a "storage" item while the item marinades, ferments or dries. The MTX item consumes 50 Power per 3 hours(consumed from socket at the time of crafting).

NYMALL'S TOOLS:

This is a requirement mod for all of my mods, and holds generic items that may be required by one or more mods. This also holds all the deffinitions for the MTX items, and some processing deffinitions I want to be availible to all of the mods.

NYMALL'S FACTIONS:

This mod adds several factions to the game, as well as strongholds and spawning rules for different groups, new currency, vendors and items relating to these groups. The additions range from serious to kooky, and meant to provide some flair to the existing game.

CONTRIBUTING:

If you wish to contribute a recipie, it must have the following information:

From:(The source the recipie is from, or a link to the website)

A itemized breakdown of the ingrediants.

Any other relevant information, such as if it needs to sit for a certain amount of time, fermentation, drying, etc.

A template called Recipie has been provided for ease of use - please apply it to any new recipies to get a idea of how things should be organized. If the issue does not at least roughly follow this template, it will be closed.

Bug reports need to have the word "BUG:" before the title. Please also give us a rundown of what you were doing before the error, and steps to reproduce, if possibe.

NOTES ON LABLES:

The FUTURE label exists to tag something for release that may not be able to make it in for that release. These are usually outlier recipies, or things that have not been decided on which in-game book they should be in. 

If the recipie is associated with a book, the book will be tagged using a label.

INCORPERATING ELEMENTS INTO YOUR MOD:

If you find something useful here that you want to use in your mod, great! Go for it! The only requirement I have is that the JSON in question have a mention of where it's from, or the Readme has a mention that "Some elements have been used from Nymall's Mods for CDDA with permission." If your mod alters something that may break this mod, please let me know and I will create an optional patch mod to repair the issue(This may only be in effect for butchery templates, as at the moment there is no way to alter without overwritting the code - let me know if anything else acts wierd and I'll take a look).

<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.com/Nymall/nymalls-foodlab-for-cdda">Nymall's Mods for CDDA</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://gitlab.com/Nymall">Nymall</a> is licensed under <a href="http://creativecommons.org/licenses/by/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">Attribution 4.0 International<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"></a></p>

What this means in summary:

This license requires that reusers give credit to the creator. It allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, even for commercial purposes.

SUPPORT:

IF you'd like to support this project, consider becoming a Patreon! Patreon supporters gain access to feature voting and some access to additional small mods! Become a Patreon by following this link: https://www.patreon.com/Nymall

I Occasionally stream development of this mod - you can join in and chat with us at https://www.twitch.tv/nymall

Content relating to the SCP Foundation, including the SCP Foundation logo, is licensed under Creative Commons Sharealike 3.0 and all concepts originate from http://www.scpwiki.com and its authors.
